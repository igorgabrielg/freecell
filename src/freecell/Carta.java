package freecell;

import java.util.Random;


public class Carta
{
	public int suit, rank;
	public static Carta[] allCards;
	public static boolean [] suitIsRed = {true, true, false, false};
	public static String[] suitLabel = { "de OURO |", "de COPAS |", "de ESPADA |", "de PAUS |" };
	public static String[] rankLabel = { "| A ", "| 2 ", "| 3 ", "| 4 ", "| 5 ", "| 6 ", "| 7 ", "| 8 ", "| 9 ", "| 10 ", "| J ", "| Q ", "| K " };


	public Carta(int rank, int suit)
	{	
		this.rank = rank;
		this.suit = suit;
	}
	
	//create a card from String reference, e.g "Ah" or "3C"
	public Carta(String str){
		
//		if (str.length() != 2){
//			throw new InvalidFormatException("Not a valid card declaration--must be 2 characters");
//		}
		
		int s,r;
		r=s=0;
		String [] arr = str.split(""); //*index 0 will be [""], so skip in loop
		for (int i = 0; i < rankLabel.length; i++) {
			if (arr[1].equals(rankLabel[i])){
				r = i;
			}
		}
		for (int j = 0; j < suitLabel.length; j++) {
			if (arr[2].equals(suitLabel[j])){
				s = j;
			}
		}
		this.suit = s;
		this.rank = r;
	}
	

	public Carta getRandomCard(){
		Random random = new Random();
		Carta card = new Carta(random.nextInt(12),random.nextInt(3));
		return card;
	}
	
	
	public String toString()
	{
		  return rankLabel[rank] + suitLabel[suit];
	}
	
	public boolean isRed(){
		if (suit == 0 || suit == 1 ){
			return true;
		}
		return false;
	}
	public int getRank() {
		 return rank;
	}

	public int getSuit() {
		return suit;
	}
	
	public static void main (String arg[]){
		String str = "Qd";
		Carta card = new Carta(str);
		System.out.println(card);
		
	}

}
