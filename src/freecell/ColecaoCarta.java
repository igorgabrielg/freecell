package freecell;
import java.util.*;


public class ColecaoCarta extends Stack <Carta> {
	

	public ColecaoCarta(){
	
	}
	
	public void pushCard(Carta card){
		push(card);
	}
	
	public Carta popTopCard(){
		if (isEmpty()) { 
			return null;
			}
		else return pop();  //pop off the top card of the column
	}
	
	public Carta peekTopCard(){
		if (isEmpty()){
		return null;
		}
		return peek();
	}
	
	public boolean playFrom(){ //return TRUE if not empty?
		if (isEmpty()){
			System.out.println("COLLECT SAYS: empty");
			return false;
		}
		return true;
	}
	
	/*Given a card to be played to the top of this card stack, 
	 * either make the legal play and return true, or ignore the illegal play and return false.
	 */
	public boolean playTo(Carta card){ //implement
		return true;
	}
	
	/*Given a stack from which the top card is to be played to the top of this card stack, 
	*either make the legal play and return true, or ignore the illegal play and return false.*/
	public boolean playTo(ColecaoCarta cards){
		return true;
	}
	
	
	public Carta[] toArray(){
		return toArray();   
	}
	
	public static void main(String[] args) {
		Deck deck = new Deck();
		Carta card = deck.drawFromDeck();
		ColecaoCarta cc = new ColecaoCarta();
		cc.pushCard(card);
		System.out.println(cc.peekTopCard()); //print top card.
		System.out.println(cc.playFrom());
		System.out.println("A carta e vermelha?? " + cc.peekTopCard().isRed());
		
	}

}
