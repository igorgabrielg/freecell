package freecell;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;


public class FreeCellPlay {
	private  Deck deck;
	//private  static String FILENAME = "moves.txt";
	public FreeCellPlay() {

	}
	public static boolean isGameOver(GameBoard b){
		boolean test = true;
		for (int i = 8; i <= 15; i++){       //are cascades 8-15 empty? game over
			if(! b.getColumn(i).isEmpty()){
				test =  false;
			}
		}
		return test;
	}
	
	public static boolean playGameByFile(GameBoard board, ArrayList<Integer> moves){
		boolean play = true;
		int source, destination;
		//Scanner parse = new Scanner(input);
			try{
				do { 
				System.out.println(board.toString());
//				 System.out.println("Enter Source and Destination column, or '0' to QUIT ");
//				 str = line.nextLine();
//				 String [] moves= str.split(" ");
	             source = moves.remove(0);
	            if (source == 0) { 
	              System.out.println("Q U I T  G A M E" );
	          	  break;
	          	}
	            destination = moves.remove(0);
	            
	            System.out.println("\nOrigem �:" + source + "\nDestino �:" + destination + "\n");
	             if(! board.isMoveLegal(source, destination)){
	            	 System.out.println("\n** Jogada Invalida, tente novamente **\n");
	            	 return play;}
	             if(isGameOver(board)){
	            	 System.out.println("\n *** Parab�ns, voc� ganhou o jogo! ***");
	             }
	             board = new GameBoard(source, destination, board);
			} while (source != 0);
				play = false;
				return play;
			} catch(ArrayIndexOutOfBoundsException e) {
				      //inform user that they have not entered two integers for source and destination
				      System.out.println("\nColoque a origem e o destino.\n");
				      return play;
			} 
	}
	
	public static boolean playGame(GameBoard board){
		boolean play = true;
		String str;
		Scanner line = new Scanner(System.in);
		int source, destination;
		//Scanner parse = new Scanner(input);
			try{
				do { 
				System.out.println(board.toString());
				 System.out.println("Digite a linha de origem e de destino para fazer a jogada ou 0 para sair.");
				 str = line.nextLine();
				 String [] moves= str.split(" ");
	             source = Integer.parseInt(moves[0]);
	            if (source == 0) { 
	              System.out.println("FIM DO JOGO" );
	          	  break;
	          	}
	            destination = Integer.parseInt(moves[1]);
	            
	            System.out.println("\nOrigem �:" + source + "\nDestino �:" + destination + "\n");
	             if(! board.isMoveLegal(source, destination)){
	            	 System.out.println("\n** Jogada Invalida, tente novamente **\n");
	            	 return play;}
	             if(isGameOver(board)){
	            	 System.out.println("\n *** Parab�ns, voc� ganhou o jogo! ***");
	             }
	             board = new GameBoard(source, destination, board);
			} while (source != 0);
				play = false;
				return play;
			} catch(ArrayIndexOutOfBoundsException e) {
				      //inform user that they have not entered two integers for source and destination
				      System.out.println("\nColoque a origem e o destino.\n");
				      return play;
			} 
	}

	public static void main(String[] args) {
		if(args.length > 0){
	        String filename = args[0].toString();
	       	try { //play with filename given as CL argument
				Scanner filescanner = new Scanner (new File(filename));
		        ArrayList<Integer> moves = new ArrayList<Integer>();
		        while(filescanner.hasNext()){
		        	moves.add(filescanner.nextInt());
		        }
		        GameBoard board = new GameBoard();
		        playGameByFile(board, moves);
		        filescanner.close();
		    } catch (FileNotFoundException e) {
		        System.err.println("O argumento " + filename + " n�o � um nome de arquivo");
		        System.exit(0);
		   }
		} else { //no filename given, play by console
		//DEBUG -- use test board
		Deck testdeck = new Deck();
		// GameBoard board = new GameBoard(testdeck); //use for debugging
		GameBoard board = new GameBoard(); //use for real game
		while(playGame(board)){
			playGame(board);
			} System.exit(0);
		}
	}
}
