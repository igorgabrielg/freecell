package freecell;


public class Livre extends ColecaoCarta {
	
	public Livre(){
	}
	
	/*can only play on empty cell*/
	public boolean playTo(Carta card){
		if(!super.isEmpty()){
			return false;
		}
		return true;
	}
	
	public boolean playTo(ColecaoCarta cards){
		if(!super.isEmpty()){
			return false;
		}
		return true;
	}
	public String toString(){
		StringBuilder result = new StringBuilder();
		result.append("Livre: ");
		for (int i = 0; i < super.size(); i++) {
			result.append( super.elementAt(i));
			result.append("  ");
		}
	return result.toString();
	}
	
	public boolean playFrom(){
		if (!super.isEmpty()){ //has 1 card, can play is true
			return true;
		}
		
		return false; //has no card, can play is false
	}
	
	public static void main(String[] args) {
		Livre cell = new Livre();
		Carta card1 = new Carta(8,3);
		Carta card2 = new Carta(7,1);
		//cell.add(card1);
		if(cell.playTo(card1)){
			cell.add(card1);
			System.out.println("Adicionado card 1");
	} else {System.out.println("Valor Falso");}

	}
}