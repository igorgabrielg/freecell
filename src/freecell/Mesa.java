package freecell;
import java.util.Stack;


public class Mesa extends ColecaoCarta  {

	private ColecaoCarta cascade;
	
	public Mesa() {
		this.cascade = new ColecaoCarta();
	}
	
	//constructor to create new Cascade with given card on top
	public Mesa (Mesa cd, Carta crd){
		cascade = cd;
	    cascade.push(crd);
		
	}


public String toString (){
		StringBuilder result = new StringBuilder();
			result.append("Mesa: ");
			for (int i = 0; i < super.size(); i++) {
				result.append( super.elementAt(i));
				result.append("  ");
			}
		return result.toString();
	}
	
	
	/*Case (1) the stack is empty, 
	 * or (2) the top card is the next higher rank with differing color.
	 */
	
	public boolean playTo(Carta card){
		if(super.isEmpty()){
			return true;
		} //rank is one higher and card is red, cascade top card is not red.
		else if((card.rank + 1) == super.peek().rank && card.isRed() == !super.peek().isRed()){
			return true;
		} //rank is one higher and card is black, cascade top card is not black.
		else if((card.rank + 1) == super.peek().rank && !card.isRed() == super.peek().isRed()){
			return true;
		}
			return false;
	}

	
	public static void main(String[] args) {
		Deck deck = new Deck();
		Mesa cd = new Mesa();
		for (int i = 0; i < 6; i++) {
			cd.pushCard(deck.drawFromDeck());
		}
		System.out.println(cd.toString());

	}

	

}
